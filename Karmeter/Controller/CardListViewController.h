//
//  CardListViewController.h
//  Karmeter
//
//  Created by Jorge Leonardo Monge García on 18/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *floatingButton;

@end
