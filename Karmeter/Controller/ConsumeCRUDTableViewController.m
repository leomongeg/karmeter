//
//  ConsumeCRUDTableViewController.m
//  Karmeter
//
//  Created by Jorge Leonardo Monge García on 20/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import "ConsumeCRUDTableViewController.h"
#import "ConsumeFormTableViewCell.h"
#import "ActionSheetDatePicker.h"
#import "Consumption.h"
#import "LMNavigationController.h"

NSString *const FORM_CONSUME_CELL_ID = @"ConsumeFormTableViewCell";
NSString *const BUTTON_CONSUME_CELL_ID = @"BUTTON_CONSUME_CELL_ID";

@interface ConsumeCRUDTableViewController ()

@property (strong, nonatomic) NSDate *dateSelected;
@property(nonatomic)BOOL invalidData;

@end

@implementation ConsumeCRUDTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.invalidData = NO;
    [self configNavbarTitle];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configNavbarTitle
{
    LMNavigationController *parentVC = (LMNavigationController *)self.parentViewController;
    
    
    if(self.consumption)
    {
        [parentVC setNavbarTitle:@"EDITAR CONSUMO"];
    }else{
        [parentVC setNavbarTitle:@"NUEVO CONSUMO"];
    }
}

#pragma mark - BIND FORM DATA

- (void)bindForm:(ConsumeFormTableViewCell *)form
{
    if(self.consumption)
    {
        
        form.txtTotal.text          = [NSString stringWithFormat:@"%.0f", self.consumption.totalAmountValue];
        form.txtLiters.text         = [NSString stringWithFormat:@"%.0f", self.consumption.quantityValue];
        form.txtDate.text           = [NSString stringWithFormat:@"%ld-%ld-%ld", self.consumption.date.year, self.consumption.date.month, self.consumption.date.day];
        form.txtAmountPerLiter.text = [NSString stringWithFormat:@"%.0f", self.consumption.amountPerQuantityValue];
        
        self.dateSelected = self.consumption.date;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 1)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:BUTTON_CONSUME_CELL_ID forIndexPath:indexPath];
        
        return cell;
    }
    
    ConsumeFormTableViewCell *cell = (ConsumeFormTableViewCell *) [tableView dequeueReusableCellWithIdentifier:FORM_CONSUME_CELL_ID forIndexPath:indexPath];
    
    if(self.invalidData) [self isValidData:cell];
    
    [self bindForm:cell];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
    {
        if(self.invalidData)
        {
            return (99*3) + 20 + self.tableView.frame.size.height * 0.48f;
        }else
        {
            return (77 * 5) + 50;
        }
    }
    
    return 36.0f;
    
    static ConsumeFormTableViewCell *sizingCell = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sizingCell = [self.tableView dequeueReusableCellWithIdentifier:FORM_CONSUME_CELL_ID];
    });
    
    sizingCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.tableView.frame), CGRectGetHeight(sizingCell.bounds));
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    
    
    return sizingCell.frame.size.height;
}

- (ConsumeFormTableViewCell *)getFormCell
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    return (ConsumeFormTableViewCell *) [self.tableView cellForRowAtIndexPath:indexPath];
}

#pragma mark - DATA VALIDATION

- (BOOL)isValidData:(ConsumeFormTableViewCell *)form
{
    BOOL valid = YES;
    
    [self updateFormErrorColor:form];
    
    if([form.txtTotal.text isWhiteSpaceOrEmty])
    {
        form.txtTotal.errorMessage = @"El monto total es requerido";
        valid = NO;
    }
    
    if([form.txtLiters.text isWhiteSpaceOrEmty])
    {
        form.txtLiters.errorMessage = @"La cantidad de litros es requerida";
        valid = NO;
    }
    
    if([form.txtDate.text isWhiteSpaceOrEmty])
    {
        form.txtDate.errorMessage = @"La fecha es requerida";
        valid = NO;
    }
    
    if([form.txtAmountPerLiter.text isWhiteSpaceOrEmty])
    {
        form.txtAmountPerLiter.errorMessage = @"El monto del valor del costo por litro es requerido";
        valid = NO;
    }
    
    return valid;
}

- (void)updateFormErrorColor:(ConsumeFormTableViewCell *)form
{
    form.txtDate.hasError             = YES;
    form.txtAmountPerLiter.hasError   = YES;
    form.txtLiters.hasError           = YES;
    form.txtTotal.hasError            = YES;
    form.txtDate.errorColor           = UIColorFromRGB(LMErrorColor);
    form.txtAmountPerLiter.errorColor = UIColorFromRGB(LMErrorColor);
    form.txtLiters.errorColor         = UIColorFromRGB(LMErrorColor);
    form.txtTotal.errorColor          = UIColorFromRGB(LMErrorColor);
}

#pragma mark - USER CALL TO ACTION

- (IBAction)saveConsume:(id)sender
{
    ConsumeFormTableViewCell * form = [self getFormCell];
    
    if(![self isValidData:form])
    {
        [self updateFormCellHeight];
        return;
    }
    
    if(!self.consumption)
    {
        self.consumption         = [Consumption MR_createEntity];
        self.consumption.vehicle = self.vehicle;
    }
    
    self.consumption.date                   = self.dateSelected;
    self.consumption.amountPerQuantityValue = [form.txtAmountPerLiter.text floatValue];
    self.consumption.quantityValue          = [form.txtLiters.text floatValue];
    self.consumption.totalAmountValue       = [form.txtTotal.text floatValue];
    
    [self.consumption.managedObjectContext MR_saveToPersistentStoreAndWait];

    LMNavigationController *parentVC = (LMNavigationController *)self.parentViewController;
    [parentVC popViewController];
}

- (void)updateFormCellHeight
{
    self.invalidData       = YES;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath]
                          withRowAnimation:UITableViewRowAnimationBottom];
    [self.tableView endUpdates];
}

- (IBAction)showDatePicker:(id)sender
{
    @weakify(self);
    [ActionSheetDatePicker showPickerWithTitle:@"Fecha"
                                datePickerMode:UIDatePickerModeDate
                                  selectedDate:[NSDate date]
                                   minimumDate:nil
                                   maximumDate:[NSDate date]
                                     doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin){
                                         @strongify(self);
                                         ConsumeFormTableViewCell *form = [self getFormCell];
                                         self.dateSelected = selectedDate;
                                         form.txtDate.text = [NSString stringWithFormat:@"%ld-%ld-%ld", self.dateSelected.year, self.dateSelected.month, self.dateSelected.day];
                                     }
                                   cancelBlock:^(ActionSheetDatePicker *picker){}
                                        origin:sender];
}

@end
