//
//  CardListViewController.m
//  Karmeter
//
//  Created by Jorge Leonardo Monge García on 18/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import "CardListViewController.h"
#import "CarTableViewCell.h"
#import "LMNavigationController.h"
#import "CustomCardTableCell.h"
#import "Vehicle.h"
#import "ConsumeListViewController.h"
#import "photoManager.h"

NSString *const CARD_CELL_ID = @"CarTableViewCell";
const CGFloat CARD_PROPORTIONAL_HEIGHT = 0.4743;

@interface CardListViewController ()<NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic)NSInteger selected;
@property (strong, nonatomic) Vehicle *vehicleSelected;
@property (nonatomic) BOOL loadVehicle;

@end

@implementation CardListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.selected    = -1;
    
    [self registerCustomCellInTableView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.loadVehicle                 = NO;
    LMNavigationController *parentVC = (LMNavigationController *)self.parentViewController;
    
    [parentVC setNavbarTitle:@"MIS VEHÍCULOS"];
    [self reloadTableData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)registerCustomCellInTableView
{
    UINib *nib = [UINib nibWithNibName:CARD_CELL_ID bundle:nil];
    [self.tableView registerNib:nib
         forCellReuseIdentifier:CARD_CELL_ID];
    
    UINib *custom = [UINib nibWithNibName:NSStringFromClass([CustomCardTableCell class]) bundle:nil];
    [self.tableView registerNib:custom
         forCellReuseIdentifier:NSStringFromClass([CustomCardTableCell class])];
}

-(void)reloadTableData
{
    [NSFetchedResultsController deleteCacheWithName:@"VehicleCache"];
    _fetchedResultsController = nil;
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error])
    {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    
    return [sectionInfo numberOfObjects];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == self.selected)
    {
        return self.tableView.frame.size.height * 0.88f;
    }
    
    return self.tableView.frame.size.height * CARD_PROPORTIONAL_HEIGHT;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.selected == indexPath.row)
    {
        CustomCardTableCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CustomCardTableCell class]) forIndexPath:indexPath];
        
        [self configureFullCard:cell atIndexPath:indexPath];
        
        if(cell.btnViewLess.allTargets.count == 0)
        {
            [cell.btnViewLess addTarget:self
                                 action:@selector(viewLess:)
                       forControlEvents:UIControlEventTouchUpInside];
        }
        
        if(cell.btnConsumption.allTargets.count == 0)
        {
            [cell.btnConsumption addTarget:self
                                    action:@selector(showConsumption:)
                          forControlEvents:UIControlEventTouchUpInside];
        }
        
        return cell;
    }
    
    CarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CARD_CELL_ID forIndexPath:indexPath];
    
    if(cell.btnConsumption.allTargets.count == 0)
    {
        [cell.btnConsumption addTarget:self
                                action:@selector(showConsumption:)
                      forControlEvents:UIControlEventTouchUpInside];
    }
    
    [self configureCard:cell atIndexPath:indexPath];
    
    if(cell.btnViewMore.allTargets.count == 0)
    {
        [cell.btnViewMore addTarget:self
                             action:@selector(showMore:)
                   forControlEvents:UIControlEventTouchUpInside];
    }
    
    return cell;
}

- (void)configureCard:(CarTableViewCell *)card atIndexPath:(NSIndexPath *)indexPath
{
    Vehicle *vehicle = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    card.lblCardName.text = [NSString stringWithFormat:@"%@ %@", vehicle.brand, vehicle.model];
    card.lblDistance.text = [NSString stringWithFormat:@"%.02f KM", vehicle.odometerValue];
    
    if(![vehicle.image isEqualToString:@"none"])
    {
        card.imgCar.image = [photoManager getPhoto:vehicle.image];
    }else
    {
        card.imgCar.image = [UIImage imageNamed:@"car-picture"];
    }
}

- (void)configureFullCard:(CustomCardTableCell *)card atIndexPath:(NSIndexPath *)indexPath
{
    Vehicle *vehicle = [self.fetchedResultsController objectAtIndexPath:indexPath];
    card.lblCardName.text = [NSString stringWithFormat:@"%@ %@", vehicle.brand, vehicle.model];
    card.lblDistance.text = [NSString stringWithFormat:@"%.02f KM", vehicle.odometerValue];
    card.lblBrand.text    = vehicle.brand;
    card.lblModel.text    = vehicle.model;
    card.lblYear.text     = [NSString stringWithFormat:@"%d", vehicle.yearValue];
    card.lblOdometer.text = [NSString stringWithFormat:@"%.02f KM", vehicle.odometerValue];
    
    if(![vehicle.image isEqualToString:@"none"])
    {
        card.imgCar.image = [photoManager getPhoto:vehicle.image];
    }else
    {
        card.imgCar.image = [UIImage imageNamed:@"car-picture"];
    }
}

#pragma mark - USER CALL TO ACTION

- (IBAction)showConsumption:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    if (indexPath != nil)
    {
        self.vehicleSelected = [self.fetchedResultsController objectAtIndexPath:indexPath];
        self.loadVehicle     = YES;
    }
    
    LMNavigationController *parentVC = (LMNavigationController *)self.parentViewController;
    [parentVC pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"ConsumeListViewController"]];
}

- (IBAction)showMore:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        if(self.selected > -1)
        {
            NSIndexPath *current = [NSIndexPath indexPathForRow:self.selected inSection:0];
            self.selected = indexPath.row;
            [self.tableView beginUpdates];
            [self.tableView reloadRowsAtIndexPaths:@[current] withRowAnimation:UITableViewRowAnimationBottom];
            [self.tableView endUpdates];
        }
        self.selected = indexPath.row;
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
        [self.tableView endUpdates];
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

- (IBAction)viewLess:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    if (indexPath != nil)
    {
        NSIndexPath *current = [NSIndexPath indexPathForRow:self.selected inSection:0];
        self.selected        = -1;
        
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:@[current] withRowAnimation:UITableViewRowAnimationBottom];
        [self.tableView endUpdates];
    }
}

- (IBAction)addNewCar:(id)sender
{
    LMNavigationController *parentVC = (LMNavigationController *)self.parentViewController;
    [parentVC pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"CardCrudFormTableViewController"]];
}

- (void)prepareForPresentViewController:(UIViewController *)viewController
{
    if([viewController isKindOfClass:[ConsumeListViewController class]] && self.loadVehicle)
    {
        ((ConsumeListViewController *)viewController).vehicle = self.vehicleSelected;
    }
}

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil)
    {
        return _fetchedResultsController;
    }
    NSFetchRequest *fetchRequest = [Vehicle MR_requestAllSortedBy:@"brand" ascending:YES];
    
    [fetchRequest setFetchLimit:100];         // Let's say limit fetch to 100
    [fetchRequest setFetchBatchSize:20];      // After 20 are faulted
    
    NSFetchedResultsController *theFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                                  managedObjectContext:[NSManagedObjectContext MR_defaultContext]
                                                                                                    sectionNameKeyPath:nil
                                                                                                             cacheName:@"VehicleCache"];
    _fetchedResultsController          = theFetchedResultsController;
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
}

@end
