//
//  ConsumeCardTableViewCell.h
//  Karmeter
//
//  Created by Jorge Leonardo Monge García on 20/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConsumeCardTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *cardView;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblLiters;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UILabel *lblCarModel;

@end
