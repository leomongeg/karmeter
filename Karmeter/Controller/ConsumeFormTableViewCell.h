//
//  ConsumeFormTableViewCell.h
//  Karmeter
//
//  Created by Jorge Leonardo Monge García on 20/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MDTextField.h"

@interface ConsumeFormTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *cardView;
@property (weak, nonatomic) IBOutlet MDTextField *txtDate;
@property (weak, nonatomic) IBOutlet MDTextField *txtLiters;
@property (weak, nonatomic) IBOutlet MDTextField *txtAmountPerLiter;
@property (weak, nonatomic) IBOutlet MDTextField *txtTotal;

@end
