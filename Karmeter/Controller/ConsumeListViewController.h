//
//  ConsumeListViewController.h
//  Karmeter
//
//  Created by Jorge Leonardo Monge García on 20/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Vehicle;

@interface ConsumeListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) Vehicle *vehicle;

@end
