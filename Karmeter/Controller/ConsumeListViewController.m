//
//  ConsumeListViewController.m
//  Karmeter
//
//  Created by Jorge Leonardo Monge García on 20/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import "ConsumeListViewController.h"
#import "ConsumeCardTableViewCell.h"
#import "LMNavigationController.h"
#import "ConsumeCRUDTableViewController.h"
#import "Vehicle.h"
#import "Consumption.h"

NSString *const CONSUME_CARD_CELL_ID = @"ConsumeCardTableViewCell";

@interface ConsumeListViewController ()<NSFetchedResultsControllerDelegate>

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) Consumption *consumptionSelected;

@end

@implementation ConsumeListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self registerCustomCellInTableView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    LMNavigationController *parentVC = (LMNavigationController *)self.parentViewController;
    [parentVC setNavbarTitle:@"MIS CONSUMOS"];
    [self reloadTableData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)registerCustomCellInTableView
{
    UINib *nib = [UINib nibWithNibName:CONSUME_CARD_CELL_ID bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:CONSUME_CARD_CELL_ID];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    
    return [sectionInfo numberOfObjects];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 111;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ConsumeCardTableViewCell *cell = (ConsumeCardTableViewCell *) [tableView dequeueReusableCellWithIdentifier:CONSUME_CARD_CELL_ID forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    if(cell.btnEdit.allTargets.count == 0)
    {
        [cell.btnEdit addTarget:self
                                action:@selector(editConsume:)
                      forControlEvents:UIControlEventTouchUpInside];
    }
    
    return cell;
}

- (void)configureCell:(ConsumeCardTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    Consumption *consumption = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    cell.lblAmount.text   = [NSString stringWithFormat:@"$%.0f", consumption.totalAmountValue];
    cell.lblLiters.text   = [NSString stringWithFormat:@"%.0f L", consumption.quantityValue];
    cell.lblDate.text     = [NSString stringWithFormat:@"%ld-%ld-%ld", consumption.date.year, consumption.date.month, consumption.date.day];
    cell.lblCarModel.text = consumption.vehicle.model;
//    cell.lblDistance.text = [NSString stringWithFormat:@"%.0f", consumption.]
}

#pragma mark - USER CALL TO ACTION

- (IBAction)editConsume:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    if (indexPath != nil)
    {
        self.consumptionSelected = [self.fetchedResultsController objectAtIndexPath:indexPath];
    }
    
    LMNavigationController *parentVC = (LMNavigationController *)self.parentViewController;
    [parentVC pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"ConsumeFormTableViewCell"]];
}

- (IBAction)addNew:(id)sender
{
    self.consumptionSelected = nil;
    
    LMNavigationController *parentVC = (LMNavigationController *)self.parentViewController;
    [parentVC pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"ConsumeFormTableViewCell"]];
}

- (void)prepareForPresentViewController:(UIViewController *)viewController
{
    if([viewController isKindOfClass:[ConsumeCRUDTableViewController class]])
    {
        ConsumeCRUDTableViewController *vc = (ConsumeCRUDTableViewController *)viewController;
        vc.vehicle                         = self.vehicle;
        
        if(self.consumptionSelected)
        {
            vc.consumption = self.consumptionSelected;
        }
    }
}

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil)
    {
        return _fetchedResultsController;
    }
    NSFetchRequest *fetchRequest = [Consumption MR_requestAllSortedBy:@"date" ascending:YES];
    
    [fetchRequest setFetchLimit:100];         // Let's say limit fetch to 100
    [fetchRequest setFetchBatchSize:20];      // After 20 are faulted
    
    if (self.vehicle)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"vehicle = %@", self.vehicle];
        fetchRequest.predicate = predicate;
    }
    
    NSFetchedResultsController *theFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                                  managedObjectContext:[NSManagedObjectContext MR_defaultContext]
                                                                                                    sectionNameKeyPath:nil
                                                                                                             cacheName:@"ConsumptionCache"];
    _fetchedResultsController          = theFetchedResultsController;
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
}

-(void)reloadTableData
{
    [NSFetchedResultsController deleteCacheWithName:@"ConsumptionCache"];
    _fetchedResultsController = nil;
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error])
    {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    
    [self.tableView reloadData];
}

@end
