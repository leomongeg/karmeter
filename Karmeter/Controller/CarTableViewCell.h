//
//  CarTableViewCell.h
//  Proyecto
//
//  Created by Jorge Leonardo Monge García on 17/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CarTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *cardView;
@property (weak, nonatomic) IBOutlet UILabel *lblCardName;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UIButton *btnConsumption;
@property (weak, nonatomic) IBOutlet UIButton *btnViewMore;
@property (weak, nonatomic) IBOutlet UIImageView *imgCar;

@end
