//
//  CarCrudFormTableViewCell.m
//  Karmeter
//
//  Created by Jorge Leonardo Monge García on 19/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import "CarCrudFormTableViewCell.h"

@implementation CarCrudFormTableViewCell

- (void)awakeFromNib
{
    [self applyShadown];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)applyShadown
{
    self.cardView.layer.shadowOffset  = CGSizeMake(1, 5);
    self.cardView.layer.shadowOpacity = 0.5;
    self.cardView.layer.shadowRadius  = 5;
}

@end
