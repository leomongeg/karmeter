//
//  CustomCardTableCell.h
//  Karmeter
//
//  Created by Jorge Leonardo Monge García on 19/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCardTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *cardView;
@property (weak, nonatomic) IBOutlet UIButton *btnViewLess;
@property (weak, nonatomic) IBOutlet UILabel *lblCardName;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblBrand;
@property (weak, nonatomic) IBOutlet UILabel *lblModel;
@property (weak, nonatomic) IBOutlet UILabel *lblYear;
@property (weak, nonatomic) IBOutlet UILabel *lblOdometer;
@property (weak, nonatomic) IBOutlet UIButton *btnConsumption;
@property (weak, nonatomic) IBOutlet UIImageView *imgCar;

@end
