//
//  CardCrudFormTableViewController.m
//  Karmeter
//
//  Created by Jorge Leonardo Monge García on 19/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import "CardCrudFormTableViewController.h"
#import "CarCrudFormTableViewCell.h"
#import "LMNavigationController.h"
#import "Vehicle.h"
#import "photoManager.h"

NSString *const FORM_CELL_ID = @"CarCrudFormTableViewCell";
NSString *const BUTTON_CELL_ID = @"CELL_BUTTON";

const CGFloat CARD_INFO_PROPORTIONAL_HEIGTH = 0.9f;

@interface CardCrudFormTableViewController ()

@property(nonatomic)BOOL invalidData;
@property (strong, nonatomic)NSString *imageSavedName;

@end

@implementation CardCrudFormTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.invalidData = NO;
    [self configNavbarTitle];
    self.imageSavedName = @"none";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configNavbarTitle
{
    LMNavigationController *parentVC = (LMNavigationController *)self.parentViewController;
    
    
    if(self.vehicle)
    {
        [parentVC setNavbarTitle:@"EDITAR VEHICULO"];
    }else{
        [parentVC setNavbarTitle:@"NUEVO VEHICULO"];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
    {
        CarCrudFormTableViewCell *cell = (CarCrudFormTableViewCell *) [tableView dequeueReusableCellWithIdentifier:FORM_CELL_ID forIndexPath:indexPath];
        
        if(self.invalidData) [self isValidData:cell];
        
        return cell;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:BUTTON_CELL_ID forIndexPath:indexPath];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
    {
        if(self.invalidData)
        {
            return (99*4) + 20 + self.tableView.frame.size.height * 0.48f;
        }else
        {
            return (77*4) + 20 + self.tableView.frame.size.height * 0.48f;
        }
    }
    
    return 36;
}

- (CarCrudFormTableViewCell *)getFormCell
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    return (CarCrudFormTableViewCell *) [self.tableView cellForRowAtIndexPath:indexPath];
}

#pragma mark - USER CALL TO ACTION

- (IBAction)selectPicture:(id)sender
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    [picker setDelegate:self];
    [picker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [picker setAllowsEditing:NO];
    
    [self presentViewController:picker animated:YES
                     completion:nil];
}

- (IBAction)save:(id)sender
{
    CarCrudFormTableViewCell * form = [self getFormCell];
    
    if(![self isValidData:form])
    {
        [self updateFormCellHeight];
        return;
    }
    
    if (!self.vehicle)
    {
        self.vehicle = [Vehicle MR_createEntity];
    }
    
    self.vehicle.model         = form.txtModel.text;
    self.vehicle.brand         = form.txtBrand.text;
    self.vehicle.yearValue     = [form.txtYear.text intValue];
    self.vehicle.odometerValue = [form.txtOdometer.text floatValue];
    self.vehicle.image         = self.imageSavedName;
    
    [self.vehicle.managedObjectContext MR_saveToPersistentStoreAndWait];
    
    LMNavigationController *parentVC = (LMNavigationController *)self.parentViewController;
    [parentVC popViewController];
}

- (void)updateFormCellHeight
{
    self.invalidData       = YES;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    [self.tableView beginUpdates];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath]
                          withRowAnimation:UITableViewRowAnimationBottom];
    [self.tableView endUpdates];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *photo = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    if(!photo) return;
    
    self.imageSavedName = [self GetUUID];
    
    @weakify(self);
    [photoManager saveImage:[photoManager fixOrientationForImage:photo]
                   withName:self.imageSavedName
             andFinishBlock:^(UIImage *savedImage){
                 @strongify(self);
                 CarCrudFormTableViewCell *card = [self getFormCell];
                 card.imgCar.image              = savedImage;
             }];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (NSString *)GetUUID
{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (__bridge NSString *)string;
}

#pragma mark - VALIDATE DATA

- (BOOL)isValidData:(CarCrudFormTableViewCell *)form
{
    BOOL valid = YES;
    
    [self updateFormErrorColor:form];
    
    if([form.txtModel.text isWhiteSpaceOrEmty])
    {
        form.txtModel.errorMessage = @"Modelo es requerido";
        valid = NO;
    }
    
    if([form.txtBrand.text isWhiteSpaceOrEmty])
    {
        form.txtBrand.errorMessage = @"La marca es requerida";
        valid = NO;
    }
    
    if([form.txtOdometer.text isWhiteSpaceOrEmty])
    {
        form.txtOdometer.errorMessage = @"El kilometraje es requerido";
        valid = NO;
    }
    
    if([form.txtYear.text isWhiteSpaceOrEmty])
    {
        form.txtYear.errorMessage = @"El año es requerido";
        valid = NO;
    }
    
    return valid;
}

- (void)updateFormErrorColor:(CarCrudFormTableViewCell *)form
{
    form.txtModel.hasError      = YES;
    form.txtBrand.hasError      = YES;
    form.txtOdometer.hasError   = YES;
    form.txtYear.hasError       = YES;
    form.txtModel.errorColor    = UIColorFromRGB(LMErrorColor);
    form.txtBrand.errorColor    = UIColorFromRGB(LMErrorColor);
    form.txtOdometer.errorColor = UIColorFromRGB(LMErrorColor);
    form.txtYear.errorColor     = UIColorFromRGB(LMErrorColor);
}



@end
