//
//  CardCrudFormTableViewController.h
//  Karmeter
//
//  Created by Jorge Leonardo Monge García on 19/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Vehicle;

@interface CardCrudFormTableViewController : UITableViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) Vehicle *vehicle;

@end
