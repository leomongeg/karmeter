//
//  CarListTableViewController.m
//  Proyecto
//
//  Created by Jorge Leonardo Monge García on 17/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import "CarListTableViewController.h"
#import "CarTableViewCell.h"
#import "LMNavigationController.h"
#import "CustomCardTableCell.h"

NSString *const CARD_CELL_IDB = @"CarTableViewCell";

@interface CarListTableViewController ()

@property (nonatomic)NSInteger selected;

@end

@implementation CarListTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.selected = 0;
    [self registerCustomCellInTableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)registerCustomCellInTableView
{
    UINib *nib = [UINib nibWithNibName:CARD_CELL_IDB bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:CARD_CELL_IDB];
    UINib *custom = [UINib nibWithNibName:NSStringFromClass([CustomCardTableCell class]) bundle:nil];
    [self.tableView registerNib:custom forCellReuseIdentifier:NSStringFromClass([CustomCardTableCell class])];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.selected == indexPath.row)
    {
        CustomCardTableCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CustomCardTableCell class]) forIndexPath:indexPath];
        
        return cell;
    }
    
    CarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CARD_CELL_IDB forIndexPath:indexPath];
    
    if(cell.btnConsumption.allTargets.count == 0)
    {
        [cell.btnConsumption addTarget:self action:@selector(showConsumption:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if(cell.btnViewMore.allTargets.count == 0)
    {
        [cell.btnViewMore addTarget:self action:@selector(showMore:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return cell;
}

#pragma mark - USER CALL TO ACTION

- (IBAction)showConsumption:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        self.selected = indexPath.row;
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];
    }
    
    return;
    LMNavigationController *parentVC = (LMNavigationController *)self.parentViewController;
    [parentVC pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"ConsumptionTableViewController"]];
}

- (IBAction)showMore:(id)sender
{
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
