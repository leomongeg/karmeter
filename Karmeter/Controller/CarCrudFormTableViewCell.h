//
//  CarCrudFormTableViewCell.h
//  Karmeter
//
//  Created by Jorge Leonardo Monge García on 19/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MDTextField.h"

@interface CarCrudFormTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *cardView;
@property (weak, nonatomic) IBOutlet UIImageView *imgCar;
@property (weak, nonatomic) IBOutlet MDTextField *txtBrand;
@property (weak, nonatomic) IBOutlet MDTextField *txtModel;
@property (weak, nonatomic) IBOutlet MDTextField *txtYear;
@property (weak, nonatomic) IBOutlet MDTextField *txtOdometer;

@end
