//
//  ConsumeCRUDTableViewController.h
//  Karmeter
//
//  Created by Jorge Leonardo Monge García on 20/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Consumption;
@class Vehicle;

@interface ConsumeCRUDTableViewController : UITableViewController

@property (strong, nonatomic) Vehicle *vehicle;
@property (strong, nonatomic) Consumption *consumption;

@end
