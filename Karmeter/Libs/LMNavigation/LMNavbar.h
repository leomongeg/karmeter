//
//  LMNavbar.h
//  Proyecto
//
//  Created by Jorge Leonardo Monge García on 16/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import <UIKit/UIKit.h>
@class HamburgerButton;

typedef NS_ENUM(NSInteger, LMNavAction) {
    LMNavActionMenu,
    LMNavActionNavigationBack
};

typedef NS_ENUM(NSInteger, LMNavbarStyle) {
    LMNavbarStyleMenu,
    LMNavbarStyleNavigation
};

@protocol LMNavbarDelegate;

@interface LMNavbar : UIView

@property (strong, nonatomic) IBOutlet UIView *statusBar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UIView *container;
@property (weak, nonatomic) IBOutlet HamburgerButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnShowMenu;
@property (assign, nonatomic) id<LMNavbarDelegate> delegate;
@property (nonatomic) LMNavAction currentAction;

- (void)showHamburgerIcon;

- (void)setBackgroundColor:(UIColor *)backgroundColor;
- (void)setTitle:(NSString *)text;

- (void)setStyle:(LMNavbarStyle)style;
- (LMNavbarStyle)getStyle;


@end

@protocol LMNavbarDelegate <NSObject>

- (void)onRequestForMenuShow:(BOOL)show;
- (void)onRequestAction:(LMNavAction)action;

@end