//
//  LMMenuItemCell.h
//  Proyecto
//
//  Created by Jorge Leonardo Monge García on 16/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LMMenuItemCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
