//
//  LMMenu.h
//  Proyecto
//
//  Created by Jorge Leonardo Monge García on 16/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LMMenuDelegate;

@interface LMMenu : UIView

@property(weak, nonatomic) id<LMMenuDelegate> delegate;

- (void)showMenu;
- (void)hideMenu;

@end

@protocol LMMenuDelegate <NSObject>

- (void)onItemSelected:(NSInteger)index;

@end