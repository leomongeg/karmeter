//
//  LMMenuTableViewController.m
//  Proyecto
//
//  Created by Jorge Leonardo Monge García on 16/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import "LMMenuTableViewController.h"
#import "LMMenuItemCell.h"

NSString *const CELL_ID = @"LMMenuItemCell";

@interface LMMenuTableViewController ()

@property(strong, nonatomic) NSArray *items;

@end

@implementation LMMenuTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.items = @[
//                   @{ @"key" : @0, @"name" : @"INICIO", @"icon" : @"menu-home" },
                   @{ @"key" : @1, @"name" : @"MIS VEHÍCULOS", @"icon" : @"menu-car" },
                   @{ @"key" : @2, @"name" : @"MIS CONSUMOS", @"icon" : @"menu-consume" },
                   @{ @"key" : @3, @"name" : @"CONFIGURACIÓN", @"icon" : @"menu-settings" }
                   ];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self registerCustomCellInTableView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)registerCustomCellInTableView
{
    UINib *nib = [UINib nibWithNibName:CELL_ID bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:CELL_ID];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LMMenuItemCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_ID forIndexPath:indexPath];
    NSDictionary *item   = [self.items objectAtIndex:indexPath.row];
    cell.imgIcon.image   = [UIImage imageNamed:[item objectForKey:@"icon"]];
    cell.lblTitle.text   = [item objectForKey:@"name"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(onMenuItemSelected:)])
    {
        [self.delegate onMenuItemSelected:indexPath.row];
    }
}

@end
