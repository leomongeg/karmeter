//
//  LMNavigationController.m
//  Proyecto
//
//  Created by Jorge Leonardo Monge García on 16/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import "LMNavigationController.h"
#import "LMNavbar.h"
#import "LMMenu.h"
#import "CarListTableViewController.h"
#import "ConsumeListViewController.h"

const CGFloat NAVBAR_PROPORTIONAL_HEIGHT = 0.095;

@interface LMNavigationController ()<LMNavbarDelegate, LMMenuDelegate>

@property (strong, nonatomic) LMMenu *menu;
@property (strong, nonatomic) UIViewController *currentVC;
@property (strong, nonatomic) UIViewController *currentViewController;
@property (strong, nonatomic) NSString *currentViewControllerID;
@property (strong, nonatomic) NSMutableArray *viewControllerQueue;

@end

@implementation LMNavigationController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.viewControllerQueue = [NSMutableArray new];
    
    [self setupViewComponents];
    
    self.currentViewController = [UIViewController new];
    
    [self addChildViewController:self.currentViewController];
    [self presentViewControllerWithIdentifier:@"CarListTableViewController"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    self.currentViewController = [UIViewController new];
    [self addChildViewController:self.currentViewController];
    [self presentViewControllerWithIdentifier:@"CarListTableViewController"];
    [self.viewControllerQueue removeAllObjects];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)setupViewComponents
{
    self.navigationBar          = [[LMNavbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height * NAVBAR_PROPORTIONAL_HEIGHT)];
    self.navigationBar.delegate = self;
    [self.navigationBar setBackgroundColor:UIColorFromRGB(LMNavbarColor)];
    
    [self.view addSubview:self.navigationBar];
    
    CGFloat cntViewY       = self.navigationBar.frame.origin.y + self.navigationBar.frame.size.height;
    CGFloat cntViewHeight  = self.view.frame.size.height - cntViewY;
    self.contentView       = [UIView new];
    self.contentView.frame = CGRectMake(0, cntViewY, self.view.frame.size.width, cntViewHeight);
    
    [self.view addSubview:self.contentView];
    [self.view bringSubviewToFront:self.navigationBar];
    
    self.menu          = [[LMMenu alloc] initWithFrame:CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, self.contentView.frame.size.height)];
    self.menu.delegate = self;
    [self.view addSubview:self.menu];

}

- (UIStatusBarStyle)preferredStatusBarStyle{ return UIStatusBarStyleLightContent; }

- (void)setNavbarTitle:(NSString *)text
{
    [self.navigationBar setTitle:text];
}

#pragma mark - Container/Content ViewController Methods


- (UIViewController *)viewControllerForIdentifier:(NSString *)vcID
{
    if([vcID isEqualToString:self.currentViewControllerID]) return nil;
    
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:vcID];;
    self.currentViewControllerID     = vcID;
    
    return viewController;
}

- (void)popViewController
{
    UIViewController *viewController;
    if(self.viewControllerQueue.count > 0)
    {
        viewController = [self.viewControllerQueue lastObject];
        [self.viewControllerQueue removeLastObject];
    }
    
    if(self.viewControllerQueue.count == 0)
    {
        [self.navigationBar setStyle:LMNavbarStyleMenu];
    }
    
    [self transitionFromViewController:self.currentViewController
                      toViewController:viewController
                              duration:0.5
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                [self.currentViewController.view removeFromSuperview];
                                viewController.view.frame = self.contentView.bounds;
                                [self.contentView addSubview:viewController.view];
                            }
                            completion:^(BOOL finished){
                                [viewController didMoveToParentViewController:self];
                                [self.currentViewController removeFromParentViewController];
                                self.currentViewController = viewController;
                            }];
}

- (void)pushViewController:(UIViewController *)viewController
{
    if(!viewController) return;
    
    [self prepareForPresentViewController:viewController];
    
    [self.viewControllerQueue addObject:self.currentViewController];
    [self.navigationBar setStyle:LMNavbarStyleNavigation];
    [self addChildViewController:viewController];
    [self transitionFromViewController:self.currentViewController
                      toViewController:viewController
                              duration:0.5
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                [self.currentViewController.view removeFromSuperview];
                                viewController.view.frame = self.contentView.bounds;
                                [self.contentView addSubview:viewController.view];
                            }
                            completion:^(BOOL finished){
                                [viewController didMoveToParentViewController:self];
                                self.currentViewController = viewController;
                            }];
}

- (void)presentViewControllerWithIdentifier:(NSString *)vcID
{
    UIViewController *viewController = [self viewControllerForIdentifier:vcID];
    
    if(!viewController) return;
    
    if(self.viewControllerQueue.count > 0)
    {
        [self.viewControllerQueue removeAllObjects];
    }
    
    [self prepareForPresentViewController:viewController];
    
    [self addChildViewController:viewController];
    [self transitionFromViewController:_currentViewController
                      toViewController:viewController
                              duration:0.5
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                [self.currentViewController.view removeFromSuperview];
                                viewController.view.frame = self.contentView.bounds;
                                [self.contentView addSubview:viewController.view];
                            }
                            completion:^(BOOL finished) {
                                [viewController didMoveToParentViewController:self];
                                [self.currentViewController removeFromParentViewController];
                                self.currentViewController = viewController;
                            }];
}

- (void)requestForPresentViewControllerWithIdentifier:(NSString *)identifier
{
    [self presentViewControllerWithIdentifier:identifier];
}

- (void)prepareForPresentViewController:(UIViewController *)viewcontroller
{
    if ([self.currentViewController respondsToSelector:@selector(prepareForPresentViewController:)])
    {
        [self.currentViewController performSelector:@selector(prepareForPresentViewController:) withObject:viewcontroller];
    }
}

#pragma mark - LMNavbarDelegate

- (void)onRequestForMenuShow:(BOOL)show
{
    show ? [self showMenu] : [self hideMenu];
}

- (void)showMenu
{
    [self.menu showMenu];
}

- (void)hideMenu
{
    [self.menu hideMenu];
}

- (void)onRequestAction:(LMNavAction)action
{
    if (action == LMNavActionNavigationBack)
    {
        [self popViewController];
    }
}

#pragma mark - LMMenuDelegate

- (void)onItemSelected:(NSInteger)index
{
    [self.navigationBar showHamburgerIcon];
    [self.menu hideMenu];
    
    switch (index) {
        case 0:
            [self presentViewControllerWithIdentifier:@"CarListTableViewController"];
            break;
        case 1:
            [self presentViewControllerWithIdentifier:@"ConsumeListViewController"];
            break;
            
        default:
            break;
    }
}

@end
