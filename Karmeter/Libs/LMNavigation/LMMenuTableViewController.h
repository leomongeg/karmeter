//
//  LMMenuTableViewController.h
//  Proyecto
//
//  Created by Jorge Leonardo Monge García on 16/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LMMenuSelectionDelegate;

@interface LMMenuTableViewController : UITableViewController

@property (weak, nonatomic) id<LMMenuSelectionDelegate> delegate;

@end

@protocol LMMenuSelectionDelegate <NSObject>

- (void)onMenuItemSelected:(NSInteger)index;

@end
