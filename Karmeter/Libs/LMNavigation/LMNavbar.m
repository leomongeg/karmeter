//
//  LMNavbar.m
//  Proyecto
//
//  Created by Jorge Leonardo Monge García on 16/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import "LMNavbar.h"
#import "Karmeter-Swift.h"

const CGFloat STATUS_BAR_HEIGHT = 20;

@interface LMNavbar()

@property(nonatomic)LMNavbarStyle navbarStyle;

@end

@implementation LMNavbar

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

#pragma mark - CONSTRUCTOR

- (instancetype)initWithFrame:(CGRect)frame //(CGSize)size
{
    self = [super initWithFrame:frame];
    
    if(self)
    {
        [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([LMNavbar class]) owner:self options:nil];
        [self setupUIComponents];
        self.navbarStyle = LMNavbarStyleMenu;
    }
    
    return self;
}

#pragma mark - UI COMPONENTS INITIALIZATION

- (void)setupUIComponents
{
    CGRect frame           = self.frame;
    self.statusBar         = [UIView new];
    self.statusBar.frame   = CGRectMake(0, 0, frame.size.width, STATUS_BAR_HEIGHT);
    
    [self addSubview:self.statusBar];
    [self.container layoutIfNeeded];
    
    self.container.frame = CGRectMake(self.container.bounds.origin.x, STATUS_BAR_HEIGHT, frame.size.width, frame.size.height);
    self.frame           = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height + STATUS_BAR_HEIGHT);
    
    [self setBackgroundColor:[UIColor grayColor]];
    [self addSubview:self.container];
    [self.container setNeedsLayout];
    [self.container layoutIfNeeded];
    [self applyShadown];
}

#pragma mark - UI CUSTOMIZATION

- (void)applyShadown
{
    self.layer.shadowOffset  = CGSizeMake(0, 0.5);
    self.layer.shadowOpacity = 0.5;
    self.layer.shadowRadius  = 5;
}

- (void)setBackgroundColor:(UIColor *)backgroundColor
{
    [super setBackgroundColor:backgroundColor];
    
    self.container.backgroundColor = backgroundColor;
    self.statusBar.backgroundColor = backgroundColor;
}

- (void)setTitle:(NSString *)text
{
    self.lblTitle.text = text;
}

#pragma mark - USER CALL TO ACTION

- (IBAction)handelMenuAction:(id)sender
{
    if(self.navbarStyle == LMNavbarStyleMenu)
    {
        [self handleMenuAppearDisappear];
    }
    else if(self.navbarStyle == LMNavbarStyleNavigation)
    {
        if(self.delegate && [self.delegate respondsToSelector:@selector(onRequestAction:)])
        {
            [self.delegate onRequestAction:LMNavActionNavigationBack];
        }
    }
}

- (void)handleMenuAppearDisappear
{
    [self.btnShowMenu setEnabled:NO];
    
    if(self.currentAction == LMNavActionMenu ||
       self.currentAction == LMNavActionNavigationBack)
    {
        NSLog(@"Show Menu %@", self.btnMenu.showsMenu ? @"YES" : @"NO");
        
        if([self.delegate respondsToSelector:@selector(onRequestForMenuShow:)])
        {
            [self.delegate onRequestForMenuShow:self.btnMenu.showsMenu];
        }
        
        self.currentAction         = self.btnMenu.showsMenu ? LMNavActionNavigationBack : LMNavActionMenu;
        self.btnMenu.showsMenu     = !self.btnMenu.showsMenu;
    }
    
    [self performSelector:@selector(enableMenuButton) withObject:nil afterDelay:0.8f];
}

- (void)enableMenuButton
{
    [self.btnShowMenu setEnabled:YES];
}

- (void)showHamburgerIcon
{
    self.btnMenu.showsMenu = YES;
}

#pragma mark - NAVIGATIONBAR STYLE

- (void)setStyle:(LMNavbarStyle)style
{
    if(self.navbarStyle == style) return;
    
    self.navbarStyle = style;
    
    if(self.navbarStyle == LMNavbarStyleMenu)
    {
        self.btnMenu.showsMenu = YES;
    }else
    {
        self.btnMenu.showsMenu = NO;
    }
}

- (LMNavbarStyle)getStyle
{
    return self.navbarStyle;
}

@end
