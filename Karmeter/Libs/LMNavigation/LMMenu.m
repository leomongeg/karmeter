//
//  LMMenu.m
//  Proyecto
//
//  Created by Jorge Leonardo Monge García on 16/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import "LMMenu.h"
#import "LMMenuTableViewController.h"

double const ANIMATION_SPEED = 0.7;

@interface LMMenu()<LMMenuSelectionDelegate>

@property (nonatomic, strong) LMMenuTableViewController *menuTBVC;

@end

@implementation LMMenu

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:CGRectMake(- frame.size.width, frame.origin.y, frame.size.width, frame.size.height)];
    
    if (self)
    {
        self.menuTBVC            = [[LMMenuTableViewController alloc] init];
        self.menuTBVC.view.frame = CGRectMake(0, 0, self.frame.size.width * 0.80, self.frame.size.height);
        self.menuTBVC.delegate   = self;
        self.backgroundColor     = [[UIColor blackColor] colorWithAlphaComponent:0.3];
        
        [self addSubview:self.menuTBVC.view];
    }
    
    return self;
}

- (void)showMenu
{
    [self showMenuContent];
}

- (void)hideMenu
{
    [self hideMenuContent];
}

- (void)showMenuContent
{
    CGRect frame = CGRectMake(self.frame.origin.x + self.frame.size.width, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
    
    [UIView animateWithDuration:ANIMATION_SPEED animations:^{
        [self setFrame:frame];
    } completion:^(BOOL finished){
    }];
}

- (void)hideMenuContent
{
    CGRect frame = CGRectMake(self.frame.origin.x - self.frame.size.width, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
    
    [UIView animateWithDuration:ANIMATION_SPEED animations:^{
        [self setFrame:frame];
    } completion:^(BOOL finished){
    }];
}

#pragma mark - LMMenuSelectionDelegate

- (void)onMenuItemSelected:(NSInteger)index
{
    if(self.delegate && [self.delegate respondsToSelector:@selector(onItemSelected:)])
    {
        [self.delegate onItemSelected:index];
    }
}

@end
