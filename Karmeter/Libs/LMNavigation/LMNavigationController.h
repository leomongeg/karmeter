//
//  LMNavigationController.h
//  Proyecto
//
//  Created by Jorge Leonardo Monge García on 16/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LMNavbar;

@interface LMNavigationController : UIViewController

@property (nonatomic, strong)UIView *contentView;
@property (nonatomic, strong)UIView *statusBarView;
@property (nonatomic, strong)LMNavbar *navigationBar;

- (void)pushViewController:(UIViewController *)viewController;
- (void)popViewController;
- (void)setNavbarTitle:(NSString *)text;

@end
