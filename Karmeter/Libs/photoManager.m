//
//  photoManager.m
//  Bayer
//
//  Created by Jorge Leonardo Monge Garcia on 11/19/13.
//  Copyright (c) 2013 LumenUp. All rights reserved.
//

#import "photoManager.h"

#define documentPath [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0]
#define imagePath @"photos"

@implementation photoManager

+ (void)saveImage:(UIImage *)image withName:(NSString *)fileName andFinishBlock:(void (^)(UIImage *imageSaved))processedImage
{
    NSString *fileFullPath = [[documentPath stringByAppendingPathComponent:imagePath] stringByAppendingPathComponent:fileName];
    
    [photoManager checkDirectory];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [UIImagePNGRepresentation(image) writeToFile:fileFullPath atomically:YES];
        if(processedImage != nil)
            dispatch_async(dispatch_get_main_queue(), ^{
                NSData *imageData = [NSData dataWithContentsOfFile:fileFullPath];
                if(imageData == nil)
                    processedImage(nil);
                else{
                    UIImage *savedImg = [UIImage imageWithData:imageData];
                    processedImage(savedImg);
                }
            });
    });
    
}

+ (void)getPhotoAsync:(NSString *)fileName andFinishBlock:(void (^)(UIImage *imageSaved))processedImage
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *fileFullPath = [[documentPath stringByAppendingPathComponent:imagePath] stringByAppendingPathComponent:fileName];
        NSData *photoData      = [NSData dataWithContentsOfFile:fileFullPath];
            dispatch_async(dispatch_get_main_queue(), ^{
                NSData *imageData = [NSData dataWithContentsOfFile:fileFullPath];
                if(photoData == nil)
                    processedImage(nil);
                else
                    processedImage([UIImage imageWithData:photoData]);
            });
    });
}

+ (UIImage *)getPhoto:(NSString *)fileName
{
    NSString *fileFullPath = [[documentPath stringByAppendingPathComponent:imagePath] stringByAppendingPathComponent:fileName];
    NSData *photoData      = [NSData dataWithContentsOfFile:fileFullPath];
    
    if(photoData == nil)
        return nil;
    
    return [UIImage imageWithData:photoData];
}

+ (void)checkDirectory
{
    NSString *path = [documentPath stringByAppendingPathComponent:imagePath];
    
    if(![[NSFileManager defaultManager] fileExistsAtPath:path])
    {
        NSError *error;
        if (![[NSFileManager defaultManager] createDirectoryAtPath:path
                                       withIntermediateDirectories:NO
                                                        attributes:nil
                                                             error:&error])
        {
            NSLog(@"Create directory error: %@", error);
        }
    }
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    //http://stackoverflow.com/a/2658801
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (UIImage *)fixOrientationForImage:(UIImage *)image
{
    
    // No-op if the orientation is already correct
    if (image.imageOrientation == UIImageOrientationUp) return image;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (image.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, image.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, image.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (image.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, image.size.width, image.size.height,
                                             CGImageGetBitsPerComponent(image.CGImage), 0,
                                             CGImageGetColorSpace(image.CGImage),
                                             CGImageGetBitmapInfo(image.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (image.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.height,image.size.width), image.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.width,image.size.height), image.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

@end
