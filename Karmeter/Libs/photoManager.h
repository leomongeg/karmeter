//
//  photoManager.h
//  Bayer
//
//  Created by Jorge Leonardo Monge Garcia on 11/19/13.
//  Copyright (c) 2013 LumenUp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface photoManager : NSObject

+ (void)saveImage:(UIImage *)image withName:(NSString *)fileName andFinishBlock:(void (^)(UIImage *imageSaved))processedImage;
+ (UIImage *)getPhoto:(NSString *)fileName;
+ (void)checkDirectory;
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
+ (UIImage *)fixOrientationForImage:(UIImage *)image;
+ (void)getPhotoAsync:(NSString *)fileName andFinishBlock:(void (^)(UIImage *imageSaved))processedImage;

@end
