//
//  AppDelegate.h
//  Karmeter
//
//  Created by Jorge Leonardo Monge García on 18/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

