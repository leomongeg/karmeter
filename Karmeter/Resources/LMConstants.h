//
//  LMConstants.h
//  Karmeter
//
//  Created by Jorge Leonardo Monge García on 19/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, LMColor)
{
    LMNavbarColor = 0x424242,
    LMErrorColor  = 0xE91E63
};

@interface LMConstants : NSObject

@end
