//
//  NSString+LMValidation.h
//  Karmeter
//
//  Created by Jorge Leonardo Monge García on 21/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (LMValidation)

- (Boolean)isWhiteSpaceOrEmty;

@end
