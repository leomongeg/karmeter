//
//  NSString+LMValidation.m
//  Karmeter
//
//  Created by Jorge Leonardo Monge García on 21/8/15.
//  Copyright (c) 2015 Digital Paradox. All rights reserved.
//

#import "NSString+LMValidation.h"

@implementation NSString (LMValidation)

- (Boolean)isWhiteSpaceOrEmty
{
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmed          = [self stringByTrimmingCharactersInSet:whitespace];
    
    return ([trimmed length] == 0) ? true : false;
}

@end
