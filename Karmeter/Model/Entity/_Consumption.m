// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Consumption.m instead.

#import "_Consumption.h"

const struct ConsumptionAttributes ConsumptionAttributes = {
	.amountPerQuantity = @"amountPerQuantity",
	.date = @"date",
	.quantity = @"quantity",
	.quantityType = @"quantityType",
	.totalAmount = @"totalAmount",
};

const struct ConsumptionRelationships ConsumptionRelationships = {
	.vehicle = @"vehicle",
};

@implementation ConsumptionID
@end

@implementation _Consumption

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Consumption" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Consumption";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Consumption" inManagedObjectContext:moc_];
}

- (ConsumptionID*)objectID {
	return (ConsumptionID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"amountPerQuantityValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"amountPerQuantity"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"quantityValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"quantity"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"totalAmountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"totalAmount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic amountPerQuantity;

- (float)amountPerQuantityValue {
	NSNumber *result = [self amountPerQuantity];
	return [result floatValue];
}

- (void)setAmountPerQuantityValue:(float)value_ {
	[self setAmountPerQuantity:@(value_)];
}

- (float)primitiveAmountPerQuantityValue {
	NSNumber *result = [self primitiveAmountPerQuantity];
	return [result floatValue];
}

- (void)setPrimitiveAmountPerQuantityValue:(float)value_ {
	[self setPrimitiveAmountPerQuantity:@(value_)];
}

@dynamic date;

@dynamic quantity;

- (float)quantityValue {
	NSNumber *result = [self quantity];
	return [result floatValue];
}

- (void)setQuantityValue:(float)value_ {
	[self setQuantity:@(value_)];
}

- (float)primitiveQuantityValue {
	NSNumber *result = [self primitiveQuantity];
	return [result floatValue];
}

- (void)setPrimitiveQuantityValue:(float)value_ {
	[self setPrimitiveQuantity:@(value_)];
}

@dynamic quantityType;

@dynamic totalAmount;

- (float)totalAmountValue {
	NSNumber *result = [self totalAmount];
	return [result floatValue];
}

- (void)setTotalAmountValue:(float)value_ {
	[self setTotalAmount:@(value_)];
}

- (float)primitiveTotalAmountValue {
	NSNumber *result = [self primitiveTotalAmount];
	return [result floatValue];
}

- (void)setPrimitiveTotalAmountValue:(float)value_ {
	[self setPrimitiveTotalAmount:@(value_)];
}

@dynamic vehicle;

@end

