// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Consumption.h instead.

@import CoreData;

extern const struct ConsumptionAttributes {
	__unsafe_unretained NSString *amountPerQuantity;
	__unsafe_unretained NSString *date;
	__unsafe_unretained NSString *quantity;
	__unsafe_unretained NSString *quantityType;
	__unsafe_unretained NSString *totalAmount;
} ConsumptionAttributes;

extern const struct ConsumptionRelationships {
	__unsafe_unretained NSString *vehicle;
} ConsumptionRelationships;

@class Vehicle;

@interface ConsumptionID : NSManagedObjectID {}
@end

@interface _Consumption : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ConsumptionID* objectID;

@property (nonatomic, strong) NSNumber* amountPerQuantity;

@property (atomic) float amountPerQuantityValue;
- (float)amountPerQuantityValue;
- (void)setAmountPerQuantityValue:(float)value_;

//- (BOOL)validateAmountPerQuantity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* date;

//- (BOOL)validateDate:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* quantity;

@property (atomic) float quantityValue;
- (float)quantityValue;
- (void)setQuantityValue:(float)value_;

//- (BOOL)validateQuantity:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* quantityType;

//- (BOOL)validateQuantityType:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* totalAmount;

@property (atomic) float totalAmountValue;
- (float)totalAmountValue;
- (void)setTotalAmountValue:(float)value_;

//- (BOOL)validateTotalAmount:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Vehicle *vehicle;

//- (BOOL)validateVehicle:(id*)value_ error:(NSError**)error_;

@end

@interface _Consumption (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveAmountPerQuantity;
- (void)setPrimitiveAmountPerQuantity:(NSNumber*)value;

- (float)primitiveAmountPerQuantityValue;
- (void)setPrimitiveAmountPerQuantityValue:(float)value_;

- (NSDate*)primitiveDate;
- (void)setPrimitiveDate:(NSDate*)value;

- (NSNumber*)primitiveQuantity;
- (void)setPrimitiveQuantity:(NSNumber*)value;

- (float)primitiveQuantityValue;
- (void)setPrimitiveQuantityValue:(float)value_;

- (NSString*)primitiveQuantityType;
- (void)setPrimitiveQuantityType:(NSString*)value;

- (NSNumber*)primitiveTotalAmount;
- (void)setPrimitiveTotalAmount:(NSNumber*)value;

- (float)primitiveTotalAmountValue;
- (void)setPrimitiveTotalAmountValue:(float)value_;

- (Vehicle*)primitiveVehicle;
- (void)setPrimitiveVehicle:(Vehicle*)value;

@end
