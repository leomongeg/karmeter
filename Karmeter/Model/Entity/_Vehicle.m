// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Vehicle.m instead.

#import "_Vehicle.h"

const struct VehicleAttributes VehicleAttributes = {
	.brand = @"brand",
	.image = @"image",
	.model = @"model",
	.odometer = @"odometer",
	.year = @"year",
};

const struct VehicleRelationships VehicleRelationships = {
	.consumptions = @"consumptions",
};

@implementation VehicleID
@end

@implementation _Vehicle

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Vehicle" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Vehicle";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Vehicle" inManagedObjectContext:moc_];
}

- (VehicleID*)objectID {
	return (VehicleID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"odometerValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"odometer"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"yearValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"year"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic brand;

@dynamic image;

@dynamic model;

@dynamic odometer;

- (float)odometerValue {
	NSNumber *result = [self odometer];
	return [result floatValue];
}

- (void)setOdometerValue:(float)value_ {
	[self setOdometer:@(value_)];
}

- (float)primitiveOdometerValue {
	NSNumber *result = [self primitiveOdometer];
	return [result floatValue];
}

- (void)setPrimitiveOdometerValue:(float)value_ {
	[self setPrimitiveOdometer:@(value_)];
}

@dynamic year;

- (int32_t)yearValue {
	NSNumber *result = [self year];
	return [result intValue];
}

- (void)setYearValue:(int32_t)value_ {
	[self setYear:@(value_)];
}

- (int32_t)primitiveYearValue {
	NSNumber *result = [self primitiveYear];
	return [result intValue];
}

- (void)setPrimitiveYearValue:(int32_t)value_ {
	[self setPrimitiveYear:@(value_)];
}

@dynamic consumptions;

- (NSMutableSet*)consumptionsSet {
	[self willAccessValueForKey:@"consumptions"];

	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"consumptions"];

	[self didAccessValueForKey:@"consumptions"];
	return result;
}

@end

