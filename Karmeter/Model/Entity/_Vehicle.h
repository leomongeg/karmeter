// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Vehicle.h instead.

@import CoreData;

extern const struct VehicleAttributes {
	__unsafe_unretained NSString *brand;
	__unsafe_unretained NSString *image;
	__unsafe_unretained NSString *model;
	__unsafe_unretained NSString *odometer;
	__unsafe_unretained NSString *year;
} VehicleAttributes;

extern const struct VehicleRelationships {
	__unsafe_unretained NSString *consumptions;
} VehicleRelationships;

@class Consumption;

@interface VehicleID : NSManagedObjectID {}
@end

@interface _Vehicle : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) VehicleID* objectID;

@property (nonatomic, strong) NSString* brand;

//- (BOOL)validateBrand:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* image;

//- (BOOL)validateImage:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* model;

//- (BOOL)validateModel:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* odometer;

@property (atomic) float odometerValue;
- (float)odometerValue;
- (void)setOdometerValue:(float)value_;

//- (BOOL)validateOdometer:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* year;

@property (atomic) int32_t yearValue;
- (int32_t)yearValue;
- (void)setYearValue:(int32_t)value_;

//- (BOOL)validateYear:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSSet *consumptions;

- (NSMutableSet*)consumptionsSet;

@end

@interface _Vehicle (ConsumptionsCoreDataGeneratedAccessors)
- (void)addConsumptions:(NSSet*)value_;
- (void)removeConsumptions:(NSSet*)value_;
- (void)addConsumptionsObject:(Consumption*)value_;
- (void)removeConsumptionsObject:(Consumption*)value_;

@end

@interface _Vehicle (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveBrand;
- (void)setPrimitiveBrand:(NSString*)value;

- (NSString*)primitiveImage;
- (void)setPrimitiveImage:(NSString*)value;

- (NSString*)primitiveModel;
- (void)setPrimitiveModel:(NSString*)value;

- (NSNumber*)primitiveOdometer;
- (void)setPrimitiveOdometer:(NSNumber*)value;

- (float)primitiveOdometerValue;
- (void)setPrimitiveOdometerValue:(float)value_;

- (NSNumber*)primitiveYear;
- (void)setPrimitiveYear:(NSNumber*)value;

- (int32_t)primitiveYearValue;
- (void)setPrimitiveYearValue:(int32_t)value_;

- (NSMutableSet*)primitiveConsumptions;
- (void)setPrimitiveConsumptions:(NSMutableSet*)value;

@end
